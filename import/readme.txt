TP Projet Site Web Toy'R'Us

Int�grer exactement comme sur la maquette.

La BDD contient:
- "brands": Marques
- "sales": Ventes par jouet et par date
- "stock": Stock disponible par jouet et par magasin
- "stores": Magasin
- "toys": Jouets (Nom, description, id marque, prix, nom du fichier de l'image)

Le site se compose de 3 pages:
- Accueil: Top 3 des meilleures ventes de l'ann�e.
- Liste: Liste des jouets avec filtre par marque.
- D�tail: D�tail d'un jouet particulier.

Images des jouets:
A stocker dans un dossier d�di�.

Menus:
- "Tous les jouets": Liste compl�te
- "Par marque": Liste pr�filtr�e sur la marque choisie (sous-menu)

D�tail:
- Information basiques du jouet
- Stock total, avec possibilit� de choisir un magasin pour affiner ce chiffre.


BONUS (� faire si tout est termin�):
- Responsive pour mobile (� imaginer)
- Dans le menu afficher le nombre de jouets pour chaque marque (ex: "Mattel (6)")
- Dans la liste:
  - Tri par prix croissant/d�croissant
  - Pagination (4 jouets par page)




cas 1 si il y a une marque et ordre croissant 
	alors affichage des jouet de la marque par ordre croissant

cas 2 si il y a une marque et ordre decroissant 
	alors affichage des jouet de la marque par ordre decroissant

cas 3 pas de marque et ordre croissant
	 alors affichage de tous les jouets par ordre de prix croissant

cas 4 pas de marque et ordre croissant
	 alors affichage de tous les jouets par ordre de prix croissant


