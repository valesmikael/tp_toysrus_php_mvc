<?php

define( 'DS', DIRECTORY_SEPARATOR );
define( 'APP_PATH', dirname( __FILE__ ) . DS );
define( 'CONTROLLER_PATH', dirname( __FILE__ ) . DS . 'controllers' . DS );
define( 'MODEL_PATH', dirname( __FILE__ ) . DS . 'models' . DS );
define( 'VIEW_PATH', dirname( __FILE__ ) . DS . 'views' . DS );
define( 'PARTIAL_VIEW_PATH', dirname( __FILE__ ) . DS . 'views' . DS . 'partials' . DS );
define( 'INC_PATH', dirname( __FILE__ ) . DS . 'inc' . DS );

define( 'APP_ROOT', 'http://toysrus.test/');
define( 'APP_NAME', 'Toy\'s\' R us');

require_once INC_PATH . 'database.php';
require_once INC_PATH . 'utils.php';

require_once CONTROLLER_PATH . 'home-controller.php';
require_once CONTROLLER_PATH . 'detailsToy-controller.php';
require_once CONTROLLER_PATH . 'allToys-controller.php';
require_once CONTROLLER_PATH . 'byBrands-controller.php';
require_once CONTROLLER_PATH . 'error-404-controller.php';

require_once INC_PATH . 'router.php';

mysqli_close( $mysqli );