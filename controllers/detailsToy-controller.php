<?php

// Page d'accueil
function detailsToyIndex()
{
    require_once MODEL_PATH . 'toys.php';
    require_once MODEL_PATH . 'stocks.php';

     // Chargement de toute les magasins
    require_once MODEL_PATH . 'stores.php';
    
    
    // Liste des résultats
    $stores = [];
    $toys = [];
    $stocks = [];
  
    
    // Traitement des données $_GET envoyées par le visiteur
    
        if(!empty($_GET['toy_id'])){
            
            $toys = getFilterToysByName( $_GET['toy_id'] );
 
        } else {
         
            echo 'Détails non disponible pour cette article';
           
        }

          // Traitement des données $_POST envoyées par le visiteur
    
        if(!empty($_POST['store_id']) && !empty($_GET['toy_id']) ){
                
            $stocks = getFilterStockByStore( $_GET['toy_id'], $_POST['store_id'] );

        } else {
        
            $stocks = getTotalStock($_GET['toy_id']);
        
        }


    $view_data = [ 'html_title' => '',
    'toys' => $toys,
    'stocks' => $stocks,
    'stores'=> getAllStores()

    ];

    loadView( 'detailsToy', $view_data );
  
}