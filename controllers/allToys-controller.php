<?php

// Page tous les jouets
function allToysIndex()
{
    
    // Import des fonctions du modèle (pour communiquer avec la BDD)
    require_once MODEL_PATH . 'toys.php';

    // Liste des résultats
    $toys = [];

    //Traitement des données $_POST envoyées par le visiteur
    
        
        if(!empty($_POST['brand_id'])&&!empty($_POST['order'])){
            
            $toys = getToysByBrandId($_POST['brand_id'],$_POST['order']);
                       
        }    elseif (!empty($_POST['brand_id'])){
            $toys = getToysByBrandId($_POST['brand_id'],$_POST['order']);

        }
            elseif (!empty($_POST['order'])){
            $toys = getAllToysByOrder($_POST['order']);

        } else {
            $toys = getAllToys();
        } 

        

    $view_data = [ 'html_title' => 'Tous les jouets',
                    'toys' => $toys

    ];

    loadView( 'allToys', $view_data);
    
    
}