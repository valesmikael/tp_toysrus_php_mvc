<?php

// Page par marque.
function byBrandsIndex()
{
        
    // Import des fonctions du modèle (pour communiquer avec la BDD)
    require_once MODEL_PATH . 'toys.php';

    // Liste des résultats
    $toys = [];
    
    // Traitement des données $_GET envoyées par le visiteur
    
        if(!empty($_POST['brand_id'])){
            $toys = getToysByBrandId( $_POST['brand_id'] );
        }
        elseif(!empty($_GET['brand_id'])){
            
            $toys = getToysByBrandId( $_GET['brand_id'] );
 
        }
            else {
            $toys = getAllToys();
           
        }
    

    $view_data = [ 'html_title' => '',
                    'toys' => $toys

    ];

    loadView( 'byBrands', $view_data);
    
}