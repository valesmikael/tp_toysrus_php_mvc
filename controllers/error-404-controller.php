<?php

// Page 404
function error404Index()
{
    header( 'HTTP/1.0 404 Not Found' );

    $view_data = [ 'html_title' => 'Page introuvable' ];

    loadView( 'error-404', $view_data );
    die();
}