<?php

// Récupère toutes les marques
function getAllBrands(): array{

    global $mysqli;

    $rows=[];
    
    $req = 'SELECT brands.*, count(*) as toy_count
            FROM brands
            JOIN toys ON toys.brand_id = brands.id
            GROUP BY brands.id';

    //Préparation
    if( $stmt = mysqli_prepare( $mysqli, $req) ) {
         
        //Execution puis récupération du resultat
        mysqli_stmt_execute( $stmt );
        $result = mysqli_stmt_get_result( $stmt );

        //Fermeture de la commande
        mysqli_stmt_close( $stmt );

        //Lecture des résultats
        while ( $row = mysqli_fetch_assoc( $result) ) {
            
            $rows[] = $row;
                        
        }
    }

    return $rows;

}

