<?php

//récupère tout les jouets
function getAllStores(): array
{
    global $mysqli;

    $rows=[];

    $req = 'SELECT * FROM stores';

    // Préparation
    if( $stmt = mysqli_prepare( $mysqli, $req) ) {

        //Execution puisrécupération du resultat
        mysqli_stmt_execute( $stmt );
        $result = mysqli_stmt_get_result( $stmt );

        //Fermeture de la commande
        mysqli_stmt_close( $stmt );

        //Lecture des resultats
        while ( $row = mysqli_fetch_assoc( $result ) ){
            // Equivalent de: array_push( $rows, $row);
            $rows[] = $row;
        }

    }
    return $rows;
}