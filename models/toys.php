<?php

function getOrderByClause( $column, $order ): string
{
    $clause = 'ORDER BY ' . $column;
    
    if( $order === 'desc' ) {
        $clause .= ' DESC';
    }
    
    return $clause;
    
}

//---------------------------------------------------------------------------------------
//récupère tout les jouets par ordre croissant
function getAllToysByOrder($order='asc'): array
{
    global $mysqli;

    $rows=[];

    $req = 'SELECT * FROM toys '
            .getOrderByClause( 'toys.price', $order);

            
    // Préparation
    if( $stmt = mysqli_prepare( $mysqli, $req) ) {

        //Execution puisrécupération du resultat
        mysqli_stmt_execute( $stmt );
        $result = mysqli_stmt_get_result( $stmt );

        //Fermeture de la commande
        mysqli_stmt_close( $stmt );

        //Lecture des resultats
        while ( $row = mysqli_fetch_assoc( $result ) ){
            // Equivalent de: array_push( $rows, $row);
            $rows[] = $row;
        }

    }
    return $rows;
}

//------------------------------------------------------------------------------------------------
 //récupère tout les jouets 
function getAllToys(): array
{
    global $mysqli;

    $rows=[];

    $req = 'SELECT * FROM toys ';

            
    // Préparation
    if( $stmt = mysqli_prepare( $mysqli, $req) ) {

        //Execution puisrécupération du resultat
        mysqli_stmt_execute( $stmt );
        $result = mysqli_stmt_get_result( $stmt );

        //Fermeture de la commande
        mysqli_stmt_close( $stmt );

        //Lecture des resultats
        while ( $row = mysqli_fetch_assoc( $result ) ){
            // Equivalent de: array_push( $rows, $row);
            $rows[] = $row;
        }

    }
    return $rows;

}

//--------------------------------------------------------------------------------------------

// Récupère le top 3 des plus vendu
function getTop3(): array{

    global $mysqli;

    $rows=[];
    
    $req = 'SELECT toy_id, `name`, `description`, price, `image`, SUM(quantity) as total
            FROM toys
            Join sales ON toys.id = toy_id
            GROUP BY toys.id
            ORDER BY total DESC
            LIMIT 3 ';
            

    //Préparation
    if( $stmt = mysqli_prepare( $mysqli, $req) ) {
         
        //Execution puis récupération du resultat
        mysqli_stmt_execute( $stmt );
        $result = mysqli_stmt_get_result( $stmt );

        //Fermeture de la commande
        mysqli_stmt_close( $stmt );

        //Lecture des résultats
        while ( $row = mysqli_fetch_assoc( $result) ) {
            
            $rows[] = $row;
                        
        }
    }

    return $rows;

}

//-------------------------------------------------------------------------------------
    function getFilterToysByName($filter):array {

        global $mysqli;
    
        $rows=[];
    
        $req ='SELECT toys.*, brands.name as brand 
        FROM toys
        JOIN brands ON brands.id = toys.brand_id 
        WHERE toys.id = ?';
        
    
    
            // Préparation
            if( $stmt = mysqli_prepare( $mysqli, $req) ) {
    
               mysqli_stmt_bind_param( $stmt, 'i', $filter);
    
                //Execution puisrécupération du resultat
                mysqli_stmt_execute( $stmt );
                $result = mysqli_stmt_get_result( $stmt );
        
                //Fermeture de la commande
                mysqli_stmt_close( $stmt );
        
                //Lecture des resultats
                while ( $row = mysqli_fetch_assoc( $result ) ){
                    // Equivalent de: array_push( $rows, $row);
                    $rows[] = $row;
                }
                
            }
            return $rows;
        }
//---------------------------------------------------------------------------
function getToysByBrandId( $brand_id, $order='asc' ): array {
    
    global $mysqli;

    $rows=[];

    $req ='SELECT * FROM toys
            WHERE brand_id = ? '
            .getOrderByClause( 'toys.price', $order);


        // Préparation
        if( $stmt = mysqli_prepare( $mysqli, $req) ) {

           mysqli_stmt_bind_param( $stmt, 'i', $brand_id);

            //Execution puisrécupération du resultat
            mysqli_stmt_execute( $stmt );
            $result = mysqli_stmt_get_result( $stmt );
    
            //Fermeture de la commande
            mysqli_stmt_close( $stmt );
    
            //Lecture des resultats
            while ( $row = mysqli_fetch_assoc( $result ) ){
                // Equivalent de: array_push( $rows, $row);
                $rows[] = $row;
            }
    
        }
        return $rows;
        
    }
    
    
