<?php


function getFilterStockByStore($filter, $filterb):array {
    
    global $mysqli;

    $row=[];

    $req ='SELECT SUM(quantity) as total_stock
            FROM stock
            WHERE toy_id = ?
            AND store_id = ? ';
    
    


        // Préparation
        if( $stmt = mysqli_prepare( $mysqli, $req) ) {

           mysqli_stmt_bind_param( $stmt, 'ii', $filter, $filterb);

            //Execution puisrécupération du resultat
            mysqli_stmt_execute( $stmt );
            $result = mysqli_stmt_get_result( $stmt );
    
            //Fermeture de la commande
            mysqli_stmt_close( $stmt );
    
            //Lecture des resultats
            $row = mysqli_fetch_assoc( $result );
           
    
        }
        return $row;
    }

    function getTotalStock($filter):array {
    
        global $mysqli;
    
        $row=[];
    
        $req ='SELECT SUM(quantity) as total_stock
                FROM stock
                WHERE toy_id = ?';      
        
    
            // Préparation
            if( $stmt = mysqli_prepare( $mysqli, $req) ) {
    
               mysqli_stmt_bind_param( $stmt, 'i', $filter);
    
                //Execution puisrécupération du resultat
                mysqli_stmt_execute( $stmt );
                $result = mysqli_stmt_get_result( $stmt );
        
                //Fermeture de la commande
                mysqli_stmt_close( $stmt );
        
                //Lecture des resultats
                $row = mysqli_fetch_assoc( $result );
               
        
            }
            return $row;
        }

