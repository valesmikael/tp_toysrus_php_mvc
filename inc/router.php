<?php

$action = isset( $_GET[ 'url' ] ) ? $_GET[ 'url' ] : '';
$arr_action = explode( '/', $action );




switch ( $arr_action[0] ) {    
   
    case '':
        homeIndex();
        break;

    case 'allToys':    
        allToysIndex();
        break;

    case 'byBrands':    
        byBrandsIndex();
        break;

    case 'detailsToy':
        detailsToyIndex();
        break;

    default: 
        error404Index();
        break;
}