<?php

// Démarrage de la session
session_start();
$session_user = $_SESSION[ 'user' ] ?? null;

define( 'DB_HOST', 'localhost' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );
define( 'DB_NAME', 'toys-r-us' );

$mysqli = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );
