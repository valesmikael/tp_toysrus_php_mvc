<?php

 // Récupère une URL absolue à partir d'un chemin donné
 function getUri( string $path ): string
 {
   return APP_ROOT . $path;
 }

// Echo une URL absolue à partir d'un chemin donné
function uri( string $path ): void
{
    echo getUri( $path );
}

// Chargement d'une view
function loadView( string $name, array $data ): void
{
     
 extract ($data); 
// Chargement de toute les marques
require_once MODEL_PATH . 'brands.php';
$brands=getAllBrands();


// Chargement de la view demandée
require_once PARTIAL_VIEW_PATH . 'header.php';
require_once VIEW_PATH . $name . '.php';
require_once PARTIAL_VIEW_PATH . 'footer.php';

}


//Redirection vers une URL donnée
function pageRedirect( string $destination ): void
{
    header( 'Location: ' . $destination );
    die();
}