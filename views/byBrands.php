<form method="post" action="" class="brands">
    <select name="brand_id">
            
        <?php foreach( $brands as $brand ): 
        // le if permet de garder le brand_id selectionner aprés la validation du formulaire
        $selected ='';
        if (!empty($_GET['brand_id'])&& $_GET['brand_id'] === (string) $brand['id']){
            $selected=' selected="selected"';
        }
        ?>
                       
        <option value="<?php echo $brand['id'];?>"<?php echo $selected;?>><?php echo $brand['name']; ?></option>

        <?php endforeach; ?>
        
    </select>   
    
    <button type="submit">Ok</button>
</form>

<!-- Boucle sur les jouets selectionner -->
<div id="list">

    <?php foreach( $toys as $toy ): ?>
    <a href="<?php uri( 'detailsToy' ); ?>/?toy_id=<?php echo $toy['id']; ?>">
        <div id="etiquette">
            <img src="../img/jouets/<?php echo $toy['image']; ?>" alt="">            
            <h4><?php echo $toy['name']; ?></h4>
            <p><?php echo $toy['price']; ?> €</p>
        </div>
    </a>
    <?php endforeach; ?>

</div>