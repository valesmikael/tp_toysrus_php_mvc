<?php foreach( $toys as $toy ): ?>

        <div id="details">
            <h4><?php echo $toy['name']; ?></h4>

            <div id="aside">

                <aside id="details-left">
                    <img src="../img/jouets/<?php echo $toy['image']; ?>" alt="">            
                    <p id="price"><?php echo $toy['price']; ?> €</p>
                    <div id="stores">
                        <form method="post" action="">
                            <select name="store_id">
                                <option value="">Quel Magasin ?</option>
                            
                                <?php foreach( $stores as $store ):
                                // le if permet de garder le brand_id selectionner aprés la validation du formulaire
                                $selected ='';
                                if (!empty($_POST['store_id'])&& $_POST['store_id'] === (string) $store['id']){
                                    $selected=' selected="selected"';
                                } ?>
                                           
                                <option value="<?php echo $store['id'];?>"<?php echo $selected;?>><?php echo $store['name']; ?></option>

                                <?php endforeach; ?>
                                
                            </select>   
                            
                            <button type="submit">Ok</button>
                        </form>
                        <p><span>Stock: </span> <?php echo $stocks['total_stock']; ?></p>
                        
                    </div>
                </aside>

                <aside id="details-right">
                    <p><span>Marque: </span><?php echo $toy['brand']; ?></p> 
                    <div id="descriptif">
                        <p><?php echo $toy['description']; ?></p>
                    </div>           
                        
                </aside>
            </div> 

            
        </div>

<?php endforeach; ?>
   


        
