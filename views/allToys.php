
<h3>Les jouets</h3>

<form method="post" action="" class="brands">
    <select name="brand_id">
        <option value="">Quelle Marque ?</option>
    
        <?php foreach( $brands as $brand ): 
        // le if permet de garder le brand_id selectionner aprés la validation du formulaire
        $selected ='';
        if (!empty($_POST['brand_id'])&& $_POST['brand_id'] === (string) $brand['id']){
            $selected=' selected="selected"';
        }
        ?>
                       
        <option value="<?php echo $brand['id'];?>"<?php echo $selected;?>><?php echo $brand['name']; ?></option>
        
        <?php endforeach; ?>
        
    </select> 
    <select name="order">
         
        <option value="">Choisir par ordre de prix</option>
        <option value="desc">Par ordre decroissant</option>
        <option value="asc">Par ordre croissant</option>
       

    </select>  
    
    <button type="submit">Ok</button>
</form>

<!-- Boucle sur les jouets selectionner -->
<div id="list">

    <?php foreach( $toys as $toy ): ?>
<a href="<?php uri( 'detailsToy' ); ?>/?toy_id=<?php echo $toy['id']; ?>">
        <div id="etiquette">
            <img src="./img/jouets/<?php echo $toy['image']; ?>" alt="">            
            <h4><?php echo $toy['name']; ?></h4>
            <p><?php echo $toy['price']; ?> €</p>
        </div>
</a>
    <?php endforeach;?>
    
</div>