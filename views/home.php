
<h3> Top 3 des Ventes</h3>   
<div id="list">

    <?php foreach( $toys as $toy ): ?>
    <a href="<?php uri( 'detailsToy' ); ?>/?toy_id=<?php echo $toy['toy_id']; ?>">
        <div id="etiquette">
            <img src="./img/jouets/<?php echo $toy['image']; ?>" alt="">            
            <h4><?php echo $toy['name']; ?></h4>
            <p><?php echo $toy['price']; ?> €</p>
        </div>
    </a>

    <?php endforeach; ?>

</div>

