<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?php echo $html_title; ?> sur <?php echo APP_NAME; ?></title>
</head>
<body>
    <header>
        <div id="logo">

            <a href="<?php uri( '' ); ?>"><img src="../img/site/logo.png" id="marque"></a>

        </div>

        <nav>
            <ul>
                <li>
                    <a href="<?php uri( 'allToys' ); ?>">Tous les jouets</a>
                </li>
                
                <li><a>Par marques <span class="fas fa-sort-down"><span></a>
                    <ul class="submenu">
                    <?php foreach( $brands as $brand ): ?>
                    
                        <li><a href="<?php uri( 'byBrands' ); ?>/?brand_id=<?php echo $brand['id']; ?>" value=""><?php echo $brand['name']; ?> (<?php echo $brand['toy_count']; ?>)</a></li>

                    <?php endforeach; ?>
                    </ul>
                </li>
                                
            </ul>
        </nav>

    </header>
   
    